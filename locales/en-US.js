export default {
  title: 'Task App',
  persian: 'Farsi',
  english: 'English',
  newTask: 'New Task',
  ReadBook: 'Read book',
  Design: 'Design',
  Learning: 'Learning',
  Programming: 'Programming',
  WhatToDo: 'What do you need to do?',
  'Note is required': 'Note is required',
  'Note must be less than 20 characters': 'Note must be less than 20 characters',
  Title: 'Title',
  'Select time': 'Select time',
  'Cancel': 'Cancel',
  'Ok': 'Ok',
  'Add your task': 'Add your task',
  'Time is required': 'Time is required',
  'Selection of time and category is required.': 'Selection of time and category is required.',
  'Description': 'Description',
  'Select alarm': 'Select alarm',
  'Set the alarm?': 'Set the alarm?',
  search: 'search',
  'Type the phrase.': 'Type the phrase.'
}
