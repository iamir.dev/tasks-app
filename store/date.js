export const state = () => ({
  init: false,
  month: 0,
  monthTitle: 'Aug',
  day: 0,
  year: 0,
  days: 0,
  mFirstDayWeek: {
    days: [],
    firstDay: 1
  }
})
export const mutations = {
  SET_MONTH(state, month) {
    state.month = month
  },
  SET_MONTH_TITLE(state, monthTitle) {
    state.monthTitle = monthTitle
  },
  SET_YEAR(state, year) {
    state.year = year
  },
  SET_DAY(state, day) {
    state.day = day
  },
  SET_INIT(state, init) {
    state.init = init
  },
  SET_DAYS(state, days) {
    state.days = days
  },
  SET_MONTH_FIRST_DAY_WEEk(state, mFirstDayWeek) {
    state.mFirstDayWeek = mFirstDayWeek
  }
}

export const actions = {
  setMonth({commit}, month) {
    commit('SET_MONTH', month)
  },
  setMonthTitle({commit}, monthTitle) {
    commit('SET_MONTH_TITLE', monthTitle)
  },
  setYear({commit}, year) {
    commit('SET_YEAR', year)
  },
  setDay({commit}, day) {
    commit('SET_DAY', day)
  },
  setDays({commit}, days) {
    commit('SET_DAYS', days)
  },
  setInit({commit}, init) {
    commit('SET_INIT', init)
  },
  setMonthFirstDayWeek({commit}, mFirstDayWeek) {
    commit('SET_MONTH_FIRST_DAY_WEEk', mFirstDayWeek)
  }
}
