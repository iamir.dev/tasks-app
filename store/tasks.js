export const state = () => ({
  newTaskCount: 0,
})
export const mutations = {
  SET_NEW_TASK_COUNT(state, newTaskCount) {
    state.newTaskCount = state.newTaskCount + newTaskCount
  },
}

export const actions = {
  setNewTaskCount({commit}, newTaskCount) {
    commit('SET_NEW_TASK_COUNT', newTaskCount)
  },
}
