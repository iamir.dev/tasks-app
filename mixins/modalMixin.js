export default {
  computed: {
    direction() {
      return this.$i18n.localeProperties.dir
    },
    backIcon() {
      return this.direction === 'rtl' ? 'mdi-arrow-right' : 'mdi-arrow-left'
    }
  }
}
