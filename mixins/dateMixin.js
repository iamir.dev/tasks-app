import {mapState} from "vuex";
import moment from "jalali-moment";

export default {
  computed: {
    ...mapState({
      year: (state) => state.date.year,
      month: (state) => state.date.month,
      monthTitle: (state) => state.date.monthTitle,
      day: (state) => state.date.day,
    }),
    calendarLocale() {
      return this.$i18n.localeProperties.time === 'jalali' ? 'fa' : 'en'
    },
    dateText() {
      if (this.calendarLocale === 'fa')
        return moment(`${this.year}_${this.month}_${this.day}`, 'jYYYY_jM_jD').format('YYYY_M_D')
      else
        return `${this.year}_${this.month}_${this.day}`
    },
  }
}
